<?php

require '../vendor/autoload.php';

$query = new \app\MDXQueryBuilder();


echo "\n~~~~~~~~~~~~~~~~~~~~~~~~ Простой MDX запрос ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";


echo $query
    ->find()
    ->select('[Measures].[Amount]')
    ->onRows('[Product].[Product].[Name]')
    ->from('[Sales]')
    ->build();

echo "\n~~~~~~~~~~~~~~~~~~~~~~~~ Применили кортеж ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

echo $query
    ->find()
    ->select('[Measures].[Amount]')
    ->onRows('([Product].[Product].[Name].&[Носок], [Product].[Product].[Name].&[Валенок])')
    ->from('[Sales]')
    ->build();

echo "\n~~~~~~~~~~~~~~~~~~~~~~~~ Применили подзапрос 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";

echo $query
    ->find()
    ->select('[Measures].[Amount]')
    ->onRows('([Product].[Product].[Name].&[Носок], [Product].[Product].[Name].&[Валенок])')
    ->subQueryOpen()
    ->select(' [Date].[Date].[Month].&[202101]')
    ->from('[Sales]')
    ->subQueryClose()
    ->build();


echo "\n~~~~~~~~~~~~~~~~~~~~~~~~ Применили подзапрос 1 ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";


echo $query
    ->find()
    ->select('[Measures].[Amount]')
    ->onRows('([Product].[Product].[Name].&[Носок], [Product].[Product].[Name].&[Валенок])')
    ->subQueryOpen()
    ->select(' [Date].[Date].[Month].&[202101]')
    ->subQueryOpen()
    ->select('[Product].[Category].[Id].&[10]')
    ->from('[Sales]')
    ->subQueryClose()
    ->subQueryClose()
    ->build();

echo "\n~~~~~~~~~~~~~~~~~~~~~~~~ Применили WITH ~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";


echo $query
    ->find()
    ->with('MySetName AS {[Measures].[Amount], [Measures].[Rest]}')
    ->select('[Measures].[Amount]')
    ->onRows('([Product].[Product].[Name].&[Носок], [Product].[Product].[Name].&[Валенок])')
    ->from('[Sales]')
    ->build();

