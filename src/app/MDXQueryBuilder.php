<?php

namespace app;

final class MDXQueryBuilder
{
    /**
     * @var string
     */
    private $mdxQuery;

    public function __construct()
    {
        $this->mdxQuery = '';
    }

    /**
     * @return $this
     */
    public function find(): self
    {
        $this->mdxQuery = '';

        return $this;
    }

    /**
     * @param string $columns
     * @return $this
     */
    public function select(string $columns): self
    {
        $this->mdxQuery =
            sprintf(
                "%s SELECT \n\t %s ON COLUMNS,\n",
                $this->mdxQuery,
                $columns
            );

        return $this;
    }

    /**
     * @param string $cube
     * @return $this
     */
    public function from(string $cube): self
    {
        $this->mdxQuery =
            sprintf(
                "%s\n FROM %s\n",
                $this->mdxQuery,
                $cube
            );

        return $this;
    }

    /**
     * @return $this
     */
    public function subQueryOpen(): self
    {
        $this->mdxQuery =
            sprintf(
                "%s FROM (\n\t",
                $this->mdxQuery
            );

        return $this;
    }

    /**
     * @return $this
     */
    public function subQueryClose(): self
    {
        $this->mdxQuery =
            sprintf(
                "%s \n) \n",
                $this->mdxQuery
            );

        return $this;
    }

    /**
     * @param string $row
     * @return $this
     */
    public function onRows(string $row): self
    {
        $this->mdxQuery =
            sprintf(
                "%s \t %s ON ROWS\n",
                $this->mdxQuery,
                $row
            );

        return $this;
    }

    /**
     * @param string $expression
     * @return $this
     */
    public function with(string $expression): self
    {
        $this->mdxQuery = sprintf(
            "%s WITH SET  %s \n",
            $this->mdxQuery,
            $expression
        );

        return $this;
    }

    /**
     * @return string
     */
    public function build(): string
    {
        return $this->mdxQuery;
    }
}



